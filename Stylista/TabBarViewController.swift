//
//  TabBarViewController.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 09.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class TabBarViewController: UITabBarController {

    let dependencies = DependenciesManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers?.forEach {
            let nc = ($0 as? NavigationController)
            nc?.dependenciesManager = dependencies
            nc?.setDependencies()
        }
    }

}
