//
//  StorageHolder.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 15.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import CoreData

final class StorageManager {
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Stylista")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                fatalError("💀 Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
}
