//
//  PiecesManager.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 02.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import CoreData
import UIKit
import Combine

protocol PiecesManagerProtocol {
    var addedItemSubject: AnyPublisher<Piece, Never> { get }
    var updatedItemSubject: AnyPublisher<Piece, Never> { get }
    var deletedItemSubject: AnyPublisher<Piece, Never> { get }

    func save(name: String, image: UIImage, tags: [String]) -> AnyPublisher<Bool, Error>
    func fetchStuff() -> AnyPublisher<[Piece], Error>
    func update(piece: Piece, withName name: String, image: UIImage, tags: [String]) -> AnyPublisher<Bool, Error>
    func delete(piece: Piece) -> AnyPublisher<Piece, Error>
    func fetchPieces(tags: [String]) -> AnyPublisher<[Piece], Error>
}

final class PiecesManager: PiecesManagerProtocol {

    private let deletedItemPassthroughSubject = PassthroughSubject<Piece, Never>()
    var deletedItemSubject: AnyPublisher<Piece, Never> {
        deletedItemPassthroughSubject.eraseToAnyPublisher()
    }

    private let updatedItemPassthroughSubject = PassthroughSubject<Piece, Never>()
    var updatedItemSubject: AnyPublisher<Piece, Never> {
        updatedItemPassthroughSubject.eraseToAnyPublisher()
    }

    private let addedItemPassthroughSubject = PassthroughSubject<Piece, Never>()
    var addedItemSubject: AnyPublisher<Piece, Never> {
        addedItemPassthroughSubject.eraseToAnyPublisher()
    }
    
    private var searchAndFilterService: SearchAndFilterServiceProtocol
    private var storage: StorageManager

    init(searchAndFilterService: SearchAndFilterServiceProtocol, storage: StorageManager) {
        self.searchAndFilterService = searchAndFilterService
        self.storage = storage
    }
    
    // MARK: - API

    func save(name: String, image: UIImage, tags: [String]) -> AnyPublisher<Bool, Error> {
        let managedContext =
            storage.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: Piece.className(),
                                       in: managedContext)!
        let piece = Piece(entity: entity,
                          insertInto: managedContext)
        
        piece.uuid = UUID().uuidString
        piece.name = name
        piece.photoData = image.png
        piece.labels = tags

        let saveAction: Action = { [weak self] in
            self?.addedItemPassthroughSubject.send(piece)
        }

        return CoreDataSaveModelPublisher(afterSaveAction: saveAction, context: managedContext).eraseToAnyPublisher()
    }
    
    func fetchStuff() -> AnyPublisher<[Piece], Error> {
        let managedContext = storage.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Piece>(entityName: Piece.className())

        return CoreDataFetchResultsPublisher(request: fetchRequest, context: managedContext, action: {}).eraseToAnyPublisher()
    }
    
    func update(piece: Piece, withName name: String, image: UIImage, tags: [String]) -> AnyPublisher<Bool, Error> {
        let managedContext = storage.persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<Piece>(entityName: Piece.className())
        fetchRequest.predicate = NSPredicate(format: "uuid = %@", piece.uuid!)

        let publisher = CoreDataFetchResultsPublisher(request: fetchRequest, context: managedContext, action: {}).flatMap { pieces -> AnyPublisher<Bool, Error> in
            let piece = pieces.first!
            piece.name = name
            piece.labels = tags
            piece.photoData = image.png
            return CoreDataSaveModelPublisher(afterSaveAction: {
                self.updatedItemPassthroughSubject.send(piece)
            }, context: managedContext).eraseToAnyPublisher()
        }.eraseToAnyPublisher()
        return publisher
    }
    
    func delete(piece: Piece) -> AnyPublisher<Piece, Error> {
        let managedContext = storage.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Piece.className())
        fetchRequest.predicate = NSPredicate(format: "uuid = %@", piece.uuid!)

        let publisher = CoreDataDeleteModelPublisher(delete: fetchRequest, context: managedContext, afterDeleteAction: { [weak self] in
            self?.deletedItemPassthroughSubject.send(piece)
        }).map { _ in
            return piece
        }.eraseToAnyPublisher()
        return publisher
    }
    
    /// loads all pieces to memory, don't call often
    func fetchPieces(tags: [String]) -> AnyPublisher<[Piece], Error> {
        let managedContext = storage.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Piece>(entityName: Piece.className())

        let publisher = CoreDataFetchResultsPublisher(request: fetchRequest, context: managedContext, action: {})
            .mapError { $0 as Error }
            .map { $0.filter{ self.piece($0, hasTags: tags) } }

        return publisher.eraseToAnyPublisher()
    }
    
    private func piece(_ piece: Piece, hasTags tags: [String]) -> Bool {
        guard let labels = piece.labels else { return false }
        var hasAllTags = true
        for tag in tags {
            if !labels.contains(tag) {
                hasAllTags = false
                break
            }
        }
        return hasAllTags
    }

    // not used
    private func deleteAll(completion: ((String?) -> Void)) {
        let managedContext = storage.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: Piece.className())
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedContext.execute(deleteRequest)
            completion(nil)
        } catch let error as NSError {
            debugPrint("💀 Could not delete. \(error), \(error.userInfo)")
            completion(error.localizedDescription)
        }
    }
}
