//
//  DependenciesManager.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 04.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class DependenciesManager {
    
    var mainModelEditing: MainModelEditingProtocol {
        MainModelEditing(piecesManager: piecesManager, searchAndFilterService: searchAndFilterService)
    }

    var mainModelViewing: MainModelViewingProtocol

    var looksAndCapsulesListProvider: LooksAndCapsulesListProvider {
        capsulesModel
    }

    var looksAndCapsulesEditor: LooksAndCapsulesEditor {
        capsulesModel
    }

    var piecesArrangementsManager: PiecesArrangementsManagerProtocol

    private var capsulesModel: LooksAndCapsulesModelProtocol

    private(set) var piecesManager: PiecesManagerProtocol!
    private(set) var searchAndFilterService: SearchAndFilterServiceProtocol = SearchAndFilterService()

    private let storage = StorageManager()
    
    init() {
        piecesManager = PiecesManager(searchAndFilterService: searchAndFilterService, storage: storage)
        mainModelViewing = MainModelEditing(piecesManager: piecesManager, searchAndFilterService: searchAndFilterService)
        piecesArrangementsManager = PiecesArrangementsManager(storage: storage)
        capsulesModel = LooksAndCapsulesModel(piecesArrangementsManager: piecesArrangementsManager, mainModel: mainModelViewing)
    }

}
