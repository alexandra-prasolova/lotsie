//
//  SearchAndFilterService.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 22.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation
import Combine

protocol SearchAndFilterServiceProtocol {
    var addedTag: AnyPublisher<TagSignal, Never> { get }
    var removedTag: AnyPublisher<TagSignal, Never> { get }
    var selectedTags: [Tag] { get }
    func addTag(_ tag: Tag)
    func removeTag(_ tag: Tag)
}

final class SearchAndFilterService: SearchAndFilterServiceProtocol {

    private let addedTagSubject = PassthroughSubject<TagSignal, Never>()
    var addedTag: AnyPublisher<TagSignal, Never> {
        return addedTagSubject.eraseToAnyPublisher()
    }

    private let removedTagSubject = PassthroughSubject<TagSignal, Never>()
    var removedTag: AnyPublisher<TagSignal, Never> {
        return removedTagSubject.eraseToAnyPublisher()
    }
    
    private(set) var selectedTags = [Tag]()
    
    func addTag(_ tag: Tag) {
        selectedTags.append(tag)
        addedTagSubject.send(TagSignal(tag: tag, index: selectedTags.count-1))
    }
    
    func removeTag(_ tag: Tag) {
        if let i = selectedTags.firstIndex(of: tag) {
            selectedTags.remove(at: i)
            removedTagSubject.send(TagSignal(tag: tag, index: i))
        }
    }
    
}
