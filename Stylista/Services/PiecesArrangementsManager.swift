//
//  PiecesArrangementsManager.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 12.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Foundation
import Combine
import CoreData

protocol PiecesArrangementsManagerProtocol {
    var addedArrangementSubject: AnyPublisher<PiecesArrangement, Never> { get }
    var updatedArrangementSubject: AnyPublisher<PiecesArrangement, Never> { get }
    var deletedArrangementSubject: AnyPublisher<PiecesArrangement, Never> { get }

    func saveArrangement(id: String?, name: String, type: String, pieces: [Piece]) -> AnyPublisher<PiecesArrangement, Error>
    func fetchArrangements() -> AnyPublisher<[PiecesArrangement], Error>
    func delete(arrangement: PiecesArrangement) -> AnyPublisher<PiecesArrangement, Error>
}

final class PiecesArrangementsManager: PiecesArrangementsManagerProtocol {

    private let addedArrangementPassthroughSubject = PassthroughSubject<PiecesArrangement, Never>()
    var addedArrangementSubject: AnyPublisher<PiecesArrangement, Never> {
        addedArrangementPassthroughSubject.eraseToAnyPublisher()
    }

    private let updatedArrangementPassthroughSubject = PassthroughSubject<PiecesArrangement, Never>()
    var updatedArrangementSubject: AnyPublisher<PiecesArrangement, Never> {
        updatedArrangementPassthroughSubject.eraseToAnyPublisher()
    }

    private let deletedArrangementPassthroughSubject = PassthroughSubject<PiecesArrangement, Never>()
    var deletedArrangementSubject: AnyPublisher<PiecesArrangement, Never> {
        deletedArrangementPassthroughSubject.eraseToAnyPublisher()
    }

    private let storage: StorageManager

    init(storage: StorageManager) {
        self.storage = storage
    }
    
    func saveArrangement(id: String?, name: String, type: String, pieces: [Piece]) -> AnyPublisher<PiecesArrangement, Error> {
        let managedContext =
            storage.persistentContainer.viewContext

        var arrangement: PiecesArrangement!
        if let id = id {
            do {
                // TODO: rewrite
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PiecesArrangement")
                fetchRequest.predicate = NSPredicate(format: "uuid = %@",
                                                     argumentArray: [id])
                let results = try managedContext.fetch(fetchRequest) as? [PiecesArrangement]
                if let arr = results?.first {
                    arrangement = arr
                }
            } catch {
                print("Fetch Failed: \(error)")
            }

        } else {
            let entity =
                NSEntityDescription.entity(forEntityName: PiecesArrangement.className(),
                                           in: managedContext)!
            arrangement = PiecesArrangement(entity: entity,
                                            insertInto: managedContext)

            arrangement.uuid = UUID().uuidString
        }

        arrangement.name = name
        arrangement.pieces = NSSet(array: pieces)
        arrangement.arrangementType = type
        pieces.forEach {
            var allArrengementsWithPiece = $0.inArrangements?.allObjects as? [PiecesArrangement] ?? []
            allArrengementsWithPiece.append(arrangement)
            $0.inArrangements = NSSet(array: allArrengementsWithPiece)
        }

        let saveAction: Action

        if id == nil {
            saveAction = { [weak self] in
                self?.addedArrangementPassthroughSubject.send(arrangement)
            }
        } else {
            saveAction = { [weak self] in
                self?.updatedArrangementPassthroughSubject.send(arrangement)
            }
        }

        let publisher = CoreDataSaveModelPublisher(afterSaveAction: saveAction, context: managedContext).map { res -> PiecesArrangement in
            return arrangement
        }

        return publisher.eraseToAnyPublisher()
    }

    func fetchArrangements() -> AnyPublisher<[PiecesArrangement], Error> {
        let managedContext = storage.persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<PiecesArrangement>(entityName: PiecesArrangement.className())

        return CoreDataFetchResultsPublisher(request: fetchRequest, context: managedContext, action: {}).eraseToAnyPublisher()
    }

    func delete(arrangement: PiecesArrangement) -> AnyPublisher<PiecesArrangement, Error> {
        let managedContext = storage.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: PiecesArrangement.className())
        fetchRequest.predicate = NSPredicate(format: "uuid = %@", arrangement.uuid!)

        let publisher = CoreDataDeleteModelPublisher(delete: fetchRequest, context: managedContext, afterDeleteAction: { [weak self] in
            self?.deletedArrangementPassthroughSubject.send(arrangement)
        }).map { [weak self] _ -> PiecesArrangement in
            self?.deletedArrangementPassthroughSubject.send(arrangement)
            return arrangement
        }.eraseToAnyPublisher()
        return publisher
    }

}
