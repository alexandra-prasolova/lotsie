//
//  Piece+Image.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 03.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

extension Piece {
    var image: UIImage? {
        if let data = photoData {
            return UIImage(data: data)
        } else {
            return nil
        }
    }
}
