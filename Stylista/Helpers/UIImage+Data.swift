//
//  UIImage+Data.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 03.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

extension UIImage {
    var png: Data? {
        guard let flattened = flattened else { return nil }
        return flattened.pngData()
    }
    var flattened: UIImage? {
        if imageOrientation == .up { return self }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: size))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

