//
//  Colors.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 23.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit


extension UIColor {
    
    static var tintColor: UIColor {
        return UIColor(named: "TintColor")!
    }
    
    static var background: UIColor {
        return UIColor(named: "Background")!
    }

    static var lighterBackground: UIColor {
        return UIColor(named: "LighterBackground")!
    }

    static var viewBackground: UIColor {
        return UIColor(named: "ViewBackground")!
    }
    
    static var secondaryBackground: UIColor {
        return UIColor(named: "SecondaryBackground")!
    }

    static var textColor: UIColor {
        return UIColor(named: "TextColor")!
    }
}
