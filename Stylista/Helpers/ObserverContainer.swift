//
//  ObserverContainer.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 21.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

public func ==<T: Hashable>(lhs: T, rhs: T) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

public struct ObserversContainer<O> {
    
    private struct Container: Hashable {
        
        weak var observer: AnyObject!
        let identifier: ObjectIdentifier
        
        init(observer: AnyObject) {
            self.observer = observer
            self.identifier = ObjectIdentifier(observer)
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
        }
    }
    
    private var containers: Set<Container> = []
    
    mutating func add(_ observer: O) {
        let observerContainer = Container(observer: observer as AnyObject)
        
        containers.insert(observerContainer)
    }

    mutating func remove(_ observer: O) {
        if let c = containers.first(where: { $0.identifier == ObjectIdentifier(observer as AnyObject) }) {
            containers.remove(c)
        }
    }
    
    func enumerate(_ enumerator: (O) -> Void) {
        containers.forEach { container in
            if let observer = container.observer as? O {
                enumerator(observer)
            }
        }
    }
    
}
