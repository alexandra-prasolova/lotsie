//
//  String+Dimentions.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 16.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

extension String {
    
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let size = CGSize(width: width, height: .greatestFiniteMagnitude)
        let attributes = [NSAttributedString.Key.font: font]
        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading]
        let rect = NSString(string: self).boundingRect(with: size, options: options, attributes: attributes, context: nil)
        return ceil(rect.height)
    }
    
    func width(height: CGFloat, font: UIFont, kern: Double = 0 ) -> CGFloat {
        let size = CGSize(width: .greatestFiniteMagnitude, height: height)
        let attributes: [NSAttributedString.Key:Any] = [.font: font, .kern: kern]
        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading]
        let rect = NSString(string: self).boundingRect(with: size, options: options, attributes: attributes, context: nil)
        return ceil(rect.width)
    }
    
}

