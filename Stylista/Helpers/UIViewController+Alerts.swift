//
//  UIViewController+Alerts.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 03.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    func prompt(title: String, subtitle: String?, actionTitle: String, destructive: Bool = false, acceptedBlock: @escaping ()->Void) {
        let alertController = UIAlertController (title: title, message: subtitle, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: actionTitle, style: destructive ? .destructive : .default) { _ in
            acceptedBlock()
        }
        
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showError(errorText: String) {
        let alertController = UIAlertController(title: "Error".localized(), message: errorText, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(messageText: String?, title: String?, completion: (EmptyBlock)? = nil) {
        let alertController = UIAlertController(title: title, message: messageText, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion?()
        })
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

