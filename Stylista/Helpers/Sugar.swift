//
//  Sugar.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 02.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

public typealias EmptyBlock = () -> Void

extension NSObject {
    
    static func className() -> String {
        let objectClass: AnyClass = self
        let objectClassName = NSStringFromClass(objectClass)
        let objectClassNameComponents = objectClassName.components(separatedBy: ".")
        return objectClassNameComponents.last!
    }
}

extension UIViewController {
    
    static var storyboardID: String {
        return className()
    }
    
}

extension UIStoryboard {
    
    func instantiate<ViewController: UIViewController>(_ viewController: ViewController.Type) -> ViewController {
        return instantiateViewController(withIdentifier: viewController.storyboardID) as! ViewController
    }
    
}

extension UIStoryboard {
    
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    static var gallery: UIStoryboard {
        return UIStoryboard(name: "Gallery", bundle: nil)
    }
    
    static var onboarding: UIStoryboard {
        return UIStoryboard(name: "Onboarding", bundle: nil)
    }
    
    static var about: UIStoryboard {
        return UIStoryboard(name: "About", bundle: nil)
    }
    
}

protocol NibLoadable: class {
    
    static var nib: UINib { get }
    
}

extension NibLoadable {
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
}

extension NibLoadable where Self: UIView {
    /**
     Returns a `UIView` object instantiated from nib
     
     - returns: A `NibLoadable`, `UIView` instance
     */
    static func loadFromNib() -> Self {
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            fatalError("The nib \(nib) expected its root view to be of type \(self)")
        }
        
        return view
    }
}

extension UICollectionViewCell {
    
    static func cellID() -> String {
        return className()
    }
}

extension UICollectionReusableView {
    
    static func viewID() -> String {
        return className()
    }
}

extension UICollectionView {
    
    func register<Cell: UICollectionViewCell>(_ cell: Cell.Type) {
        let nib = UINib(nibName: cell.className(), bundle: nil)
        register(nib, forCellWithReuseIdentifier: cell.cellID())
    }
    
    func register<View: UICollectionReusableView>(_ view: View.Type) {
        let nib = UINib(nibName: view.className(), bundle: nil)
        register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: view.viewID())
    }
    
    func dequeue<Cell: UICollectionViewCell>(at indexPath: IndexPath, cell: Cell.Type = Cell.self) -> Cell {
        let cell = dequeueReusableCell(withReuseIdentifier: cell.cellID(), for: indexPath) as? Cell
        return cell!
    }
    
    func dequeue<View: UICollectionReusableView>(at indexPath: IndexPath, of kind: String = UICollectionView.elementKindSectionHeader, view: View.Type = View.self) -> View {
        let cell = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: view.viewID(), for: indexPath) as? View
        return cell!
    }
}

extension UITableViewCell {
    
    static func cellID() -> String {
        return className()
    }
}

extension UITableViewHeaderFooterView {
    
    static func viewID() -> String {
        return className()
    }
}

extension UITableView {
    
    func register<View: UITableViewHeaderFooterView>(_ view: View.Type) {
        let nib = UINib(nibName: view.className(), bundle: nil)
        register(nib, forHeaderFooterViewReuseIdentifier: view.viewID())
    }
    
    func register<Cell: UITableViewCell>(_ cell: Cell.Type) {
        let nib = UINib(nibName: cell.className(), bundle: nil)
        register(nib, forCellReuseIdentifier: cell.cellID())
    }
    
    func dequeue<Cell: UITableViewCell>(_ cell: Cell.Type = Cell.self) -> Cell {
        let cell = dequeueReusableCell(withIdentifier: cell.cellID()) as? Cell
        return cell!
    }
    
    func dequeue<Cell: UITableViewCell>(at indexPath: IndexPath, cell: Cell.Type = Cell.self) -> Cell {
        let cell = dequeueReusableCell(withIdentifier: cell.cellID(), for: indexPath) as? Cell
        return cell!
    }
    
    func dequeue<View: UITableViewHeaderFooterView>(_ view: View.Type = View.self) -> View {
        return dequeueReusableHeaderFooterView(withIdentifier: view.viewID()) as! View
    }
    
}

extension String {
    
    public func localized(arguments: CVarArg..., comment: String = "") -> String {
        if arguments.isEmpty {
            return NSLocalizedString(self, comment: comment)
        } else {
            return withVaList(arguments) { arguments -> String in
                let format = NSLocalizedString(self, comment: comment)
                return NSString(format: format, arguments: arguments) as String
            }
        }
    }
    
}
