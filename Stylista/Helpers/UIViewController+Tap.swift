//
//  UIViewController+Tap.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 15.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func startWatchingTapOutsideTextInput() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
        view.addGestureRecognizer(tap)
    }
    
    @objc
    private func tapView(_ sender: UIPanGestureRecognizer) {
        if !(sender.view is UITextInput) {
            view.endEditing(true)
        }
    }
    
}
