//
//  UIImagePickerController+PhotoAndLibrary.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 03.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

extension UIImagePickerController {
    
    static var forPhoto: UIImagePickerController {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.cameraCaptureMode = .photo
        vc.cameraOverlayView = nil
        return vc
    }
    
    static var forLibrary: UIImagePickerController {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        return vc
    }
    
}
