//
//  LooksAndCapsulesVC.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 12.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import UIKit
import Combine

final class LooksAndCapsulesVC: UIViewController {

    var dependenciesManager: DependenciesManager!

    var arrangementsListProvider: LooksAndCapsulesListProvider!

    @IBOutlet private var typesSegmentControl: UISegmentedControl!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var emptyStateLabel: UILabel!

    private var disposables = Set<AnyCancellable>()

    private var cellSide: CGFloat = 100

    private var selectedType: ArrangementType = .look {
        didSet {
            collectionView.reloadData()
        }
    }
    private var arrangements: [PiecesArrangement] {
        return arrangementsListProvider.arrangements[selectedType] ?? []
    }

    private var selectedArrangement: PiecesArrangement?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Looks and Capsules".localized()

        collectionView.register(LooksAndCapsulesCell.self)

        setupSegmantControl()
        subscribeToStuff()
        setSelected(segment: 0)
    }

    private func setupSegmantControl() {
        typesSegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.background], for: .selected)
        typesSegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.secondaryBackground], for: .normal)
        typesSegmentControl.setTitle("Looks".localized(), forSegmentAt: 0)
        typesSegmentControl.setTitle("Capsules".localized(), forSegmentAt: 1)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let count = 2
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let space = layout.minimumInteritemSpacing
        let insets = layout.sectionInset.left + layout.sectionInset.right
        cellSide = (collectionView.bounds.width - space * CGFloat(count - 1) - insets) / CGFloat(count)
    }

    private func subscribeToStuff() {
        arrangementsListProvider.addedArrangementSubject.sink { [weak self] _ in
            self?.collectionView.reloadData()
        }.store(in: &disposables)

        arrangementsListProvider.updatedArrangementSubject.sink { [weak self] _ in
            self?.collectionView.reloadData()
        }.store(in: &disposables)

        arrangementsListProvider.deletedArrangementSubject.sink { [weak self] _ in
            self?.collectionView.reloadData()
        }.store(in: &disposables)

        arrangementsListProvider.fetchArrangements().sink(receiveCompletion: { [weak self] end in
            switch end {
            case .finished: break
            case .failure(let error):
                self?.showError(errorText: error.localizedDescription)
            }
        }, receiveValue: { [weak self] res in
            self?.collectionView.reloadData()
        }).store(in: &disposables)
    }

    private func setSelected(segment: Int) {
        switch segment {
        case 0:
            emptyStateLabel.text = "No looks yet".localized()
            selectedType = .look
        case 1:
            emptyStateLabel.text = "No capsules yet".localized()
            selectedType = .capsule
        default:
            break
        }
        emptyStateLabel.isHidden = !arrangements.isEmpty
    }

    @IBAction private func switchedSegment(_ sender: UISegmentedControl) {
        setSelected(segment: sender.selectedSegmentIndex)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toAddArrangement":
            if let vc = segue.destination as? AddArrangementVC {
                vc.dependenciesManager = dependenciesManager
                vc.model = dependenciesManager.looksAndCapsulesEditor
                vc.mode = .add
            }
        case "toViewArrangement":
            if let vc = segue.destination as? AddArrangementVC, let arrangement = selectedArrangement {
                vc.dependenciesManager = dependenciesManager
                vc.model = dependenciesManager.looksAndCapsulesEditor
                vc.mode = .view(arrangement: arrangement)
                selectedArrangement = nil
            }
        default:
            break
        }
    }

}

extension LooksAndCapsulesVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSide, height: cellSide)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedArrangement = arrangements[indexPath.item]
        performSegue(withIdentifier: "toViewArrangement", sender: nil)
    }
}

extension LooksAndCapsulesVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrangements.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(at: indexPath, cell: LooksAndCapsulesCell.self)
        cell.fill(title: arrangements[indexPath.item].name ?? "No name")
        return cell
    }
}
