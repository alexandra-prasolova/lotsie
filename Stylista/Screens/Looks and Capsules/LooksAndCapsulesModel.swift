//
//  LooksAndCapsulesModel.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 12.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Foundation
import Combine

enum ArrangementType: String {
    case look = "Look"
    case capsule = "Capsule"

    static var all: [ArrangementType] {
        return [.look, .capsule]
    }
}

protocol LooksAndCapsulesListProvider {
    var arrangements: [ArrangementType: [PiecesArrangement]] { get }
    var addedArrangementSubject: AnyPublisher<PiecesArrangement, Never> { get }
    var updatedArrangementSubject: AnyPublisher<PiecesArrangement, Never> { get }
    var deletedArrangementSubject: AnyPublisher<PiecesArrangement, Never> { get }
    func fetchArrangements() -> AnyPublisher<[PiecesArrangement], Error>
}

protocol LooksAndCapsulesEditor {
    var selectedItemsPublisher: AnyPublisher<[Piece], Never> { get }
    var selectedItems: [Piece] { get }
    func addArrangement(type: ArrangementType, name: String, id: String?) -> AnyPublisher<PiecesArrangement, Error>
    func set(arrangement: PiecesArrangement?)
    func delete(arrangement: PiecesArrangement) -> AnyPublisher<PiecesArrangement, Error>
}

protocol LooksAndCapsulesModelProtocol: LooksAndCapsulesEditor, LooksAndCapsulesListProvider {}

final class LooksAndCapsulesModel: LooksAndCapsulesModelProtocol {
    var selectedItemsPublisher: AnyPublisher<[Piece], Never> {
        return mainModel.selectedItemsPublisher
    }

    var selectedItems: [Piece] { mainModel.selectedItems }

    private let addedArrangementSubjectPrivate = PassthroughSubject<PiecesArrangement, Never>()
    var addedArrangementSubject: AnyPublisher<PiecesArrangement, Never> {
        addedArrangementSubjectPrivate.eraseToAnyPublisher()
    }

    private let updatedArrangementSubjectPrivate = PassthroughSubject<PiecesArrangement, Never>()
    var updatedArrangementSubject: AnyPublisher<PiecesArrangement, Never> {
        updatedArrangementSubjectPrivate.eraseToAnyPublisher()
    }

    private var deletedArrangementSubjectPrivate = PassthroughSubject<PiecesArrangement, Never>()
    var deletedArrangementSubject: AnyPublisher<PiecesArrangement, Never> {
        deletedArrangementSubjectPrivate.eraseToAnyPublisher()
    }

    private(set) var arrangements: [ArrangementType: [PiecesArrangement]] = [:]
    private var mainModel: MainModelViewingProtocol
    private let piecesArrangementsManager: PiecesArrangementsManagerProtocol

    private var disposables = Set<AnyCancellable>()

    init(piecesArrangementsManager: PiecesArrangementsManagerProtocol, mainModel: MainModelViewingProtocol) {
        self.piecesArrangementsManager = piecesArrangementsManager
        self.mainModel = mainModel

        piecesArrangementsManager.addedArrangementSubject.map({ [weak self] sub in
            switch sub.arrangementType!.lowercased() {
            case ArrangementType.look.rawValue.lowercased():
                self?.arrangements[ArrangementType.look]?.append(sub)
            case ArrangementType.capsule.rawValue.lowercased():
                self?.arrangements[ArrangementType.capsule]?.append(sub)
            default: break
            }
            return sub
        }).sink(receiveValue: { [weak self] res in
            self?.addedArrangementSubjectPrivate.send(res)
        }).store(in: &disposables)

        piecesArrangementsManager.updatedArrangementSubject.map({ [weak self] sub in
            var arrs: [PiecesArrangement] = []
            switch sub.arrangementType!.lowercased() {
            case ArrangementType.look.rawValue.lowercased():
                arrs = self?.arrangements[ArrangementType.look] ?? []
            case ArrangementType.capsule.rawValue.lowercased():
                arrs = self?.arrangements[ArrangementType.capsule] ?? []
            default: break
            }
            if let i = arrs.firstIndex(where: {$0.uuid == sub.uuid}) {
                arrs[i] = sub
            }
            return sub
        }).sink(receiveValue: { [weak self] res in
            self?.updatedArrangementSubjectPrivate.send(res)
        }).store(in: &disposables)


        piecesArrangementsManager.deletedArrangementSubject.sink {[weak self] arr in
            switch arr.arrangementType!.lowercased() {
            case ArrangementType.look.rawValue.lowercased():
                if let i = self?.arrangements[ArrangementType.look]?.firstIndex(of: arr) {
                    self?.arrangements[ArrangementType.look]?.remove(at: i)
                }
            case ArrangementType.capsule.rawValue.lowercased():
                if let i = self?.arrangements[ArrangementType.capsule]?.firstIndex(of: arr) {
                    self?.arrangements[ArrangementType.capsule]?.remove(at: i)
                }
            default: break
            }
            self?.deletedArrangementSubjectPrivate.send(arr)
        }.store(in: &disposables)
    }

    func set(arrangement: PiecesArrangement?) {
        mainModel.selectedItems = arrangement?.piecesArray ?? []
    }

    func addArrangement(type: ArrangementType, name: String, id: String?) -> AnyPublisher<PiecesArrangement, Error> {
        piecesArrangementsManager.saveArrangement(id: id, name: name, type: type.rawValue, pieces: mainModel.selectedItems)
    }

    func delete(arrangement: PiecesArrangement) -> AnyPublisher<PiecesArrangement, Error> {
        piecesArrangementsManager.delete(arrangement: arrangement).map { _ in
            return arrangement
        }.eraseToAnyPublisher()
    }

    func fetchArrangements() -> AnyPublisher<[PiecesArrangement], Error> {
        piecesArrangementsManager.fetchArrangements().map { [weak self] fetchedArrangements in
            var looks = [PiecesArrangement]()
            var capsules = [PiecesArrangement]()
            fetchedArrangements.forEach({
                switch $0.arrangementType!.lowercased() {
                case ArrangementType.look.rawValue.lowercased():
                    looks.append($0)
                case ArrangementType.capsule.rawValue.lowercased():
                    capsules.append($0)
                default: break
                }
            })
            self?.arrangements[.capsule] = capsules
            self?.arrangements[.look] = looks
            return fetchedArrangements
        }.eraseToAnyPublisher()
    }
}
