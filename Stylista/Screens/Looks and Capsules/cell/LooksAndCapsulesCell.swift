//
//  LooksAndCapsulesCell.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 12.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class LooksAndCapsulesCell: UICollectionViewCell {

    @IBOutlet private var titleLabel: UILabel!

    func fill(title: String) {
        titleLabel.text = title
    }

}
