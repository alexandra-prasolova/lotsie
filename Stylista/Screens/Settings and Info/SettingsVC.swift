//
//  SettingsVC.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 02.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import MessageUI

final class SettingsVC: UIViewController {
    
    private struct AboutData {
        let title: String
        let contentType: TermsViewController.ContentType?
        
        init(title: String, contentType: TermsViewController.ContentType? = nil) {
            self.title = title
            self.contentType = contentType
        }
    }
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var versionLabel: UILabel!
    
    private var selected: Int?
    
    private var data: [AboutData] = [AboutData(title: "Terms and Conditions".localized(), contentType: .tos),
                                     AboutData(title: "Privacy Policy".localized(), contentType: .pp),
                                     AboutData(title: "Email Author".localized())]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Settings&Info".localized()
        setAppVersion()
        tableView.register(AboutTableViewCell.self)
        
        navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func setAppVersion() {
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionLabel.text = "Lotsie " + appVersion
        }
    }
    
    private func openMail() {
        guard MFMailComposeViewController.canSendMail() else { return }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        composeVC.setToRecipients(["alexandra.volnaya@gmail.com"])
        composeVC.setSubject("Lotsie")
        composeVC.setMessageBody("", isHTML: false)
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toTermsViewController":
            guard let s = selected, let c = data[s].contentType else { return }
            let vc = segue.destination as! TermsViewController
            vc.contentType = c
        default:
            break
        }
        selected = nil
    }
    
}

extension SettingsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = indexPath.row
        switch indexPath.row {
        case 0...1:
            performSegue(withIdentifier: "toTermsViewController", sender: nil)
        case 2:
            openMail()
        default:
            break
        }
    }
    
}

extension SettingsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(at: indexPath, cell: AboutTableViewCell.self)
        cell.titleLabel?.text = data[indexPath.row].title
        
        return cell
    }
    
}

extension SettingsVC: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        if let errorText = error?.localizedDescription {
            showError(errorText: errorText)
        }
    }
    
}

