//
//  TermsViewController.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 03.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class TermsViewController: UIViewController {
    
    enum ContentType: String {
        case tos = "ToS"
        case pp = "PP"
    }
    
    @IBOutlet var textView: UITextView!
    
    var contentType: ContentType = .tos

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = contentType.rawValue.localized()
        
        switch contentType {
        case .pp:
            navigationItem.title = NSLocalizedString("Privacy Policy", comment: "")
        case .tos:
            navigationItem.title = NSLocalizedString("Terms and Conditions", comment: "")
        }
    }
    
}
