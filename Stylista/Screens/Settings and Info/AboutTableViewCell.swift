//
//  AboutTableViewCell.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 03.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class AboutTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
}
