//
//  AddArrangementVC.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 12.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import UIKit
import Combine

final class AddArrangementVC: UIViewController {

    enum Mode {
        case add
        case edit(arrangement: PiecesArrangement)
        case view(arrangement: PiecesArrangement)
    }

    var dependenciesManager: DependenciesManager!
    var model: LooksAndCapsulesEditor!
    var mode: Mode = .add
    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var arrangementTypeTextField: UITextField!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var pickPiecesButton: UIButton!
    @IBOutlet private var emptyStateLabel: UILabel!

    private let typePicker = UIPickerView()

    private var pieces: [Piece] {
        model.selectedItems
    }

    private let allTypes = ArrangementType.all
    private var selectedType = CurrentValueSubject<ArrangementType, Never>(.capsule)

    private var disposables = Set<AnyCancellable>()

    private var cellSide: CGFloat = 100

    private var deleteItem: UIBarButtonItem { UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(trashArrangement))}

    private var cancelItem: UIBarButtonItem { UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(editCancelTapped))}

    private var editItem: UIBarButtonItem {
        let item = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editTapped))
        return item
    }

    private var saveItem: UIBarButtonItem {
        let item = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTapped))
        return item
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(PieceCellLarge.self)
        setupTypePicker()
        startWatchingTapOutsideTextInput()
        subscribeToStuff()
        pickPiecesButton.setTitle("Pick pieces".localized(), for: .normal)
        set(mode: mode)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutCollection()
    }

    private func setupTypePicker() {
        typePicker.delegate = self
        typePicker.dataSource = self
        arrangementTypeTextField.inputView = typePicker
        arrangementTypeTextField.delegate = self
        let row = allTypes.firstIndex(of: selectedType.value) ?? 0
        typePicker.selectRow(row, inComponent: 0, animated: false)
        typePicker.backgroundColor = .secondaryBackground
    }

    private func set(mode: Mode) {
        self.mode = mode
        switch mode {
        case .add:
            pickPiecesButton.isHidden = false
            nameTextField.isUserInteractionEnabled = true
            arrangementTypeTextField.isUserInteractionEnabled = true
            model.set(arrangement: nil)
            navigationItem.setRightBarButtonItems([saveItem], animated: true)
            navigationItem.setLeftBarButton(navigationItem.backBarButtonItem, animated: true)
        case .edit(arrangement: let arrangement):
            model.set(arrangement: arrangement)
            fill(with: arrangement)
            pickPiecesButton.isHidden = false
            nameTextField.isUserInteractionEnabled = true
            arrangementTypeTextField.isUserInteractionEnabled = true
            navigationItem.setRightBarButtonItems([saveItem], animated: true)
            navigationItem.setLeftBarButton(cancelItem, animated: true)
        case .view(arrangement: let arrangement):
            model.set(arrangement: arrangement)
            pickPiecesButton.isHidden = true
            nameTextField.isUserInteractionEnabled = false
            arrangementTypeTextField.isUserInteractionEnabled = false
            navigationItem.setRightBarButtonItems([editItem, deleteItem], animated: true)
            navigationItem.setLeftBarButton(navigationItem.backBarButtonItem, animated: true)
            fill(with: arrangement)
        }
    }

    @objc private func trashArrangement(_ sender: Any) {
        prompt(title: "Confirm deletion", subtitle: "Do you want to delete \(nameTextField.text ?? "arrangement")?", actionTitle: "Delete", destructive: true, acceptedBlock: {
            self.delete()
        })
    }

    @objc private func editCancelTapped() {
        switch mode {
        case .edit(arrangement: let arrangement):
            set(mode: .view(arrangement: arrangement))
        default:
            break
        }
    }

    @objc private func editTapped() {
        switch mode {
        case .view(arrangement: let arrangement):
            set(mode: .edit(arrangement: arrangement))
        default:
            break
        }
    }

    @objc private func saveTapped() {
        var id: String?
        switch mode {
        case .edit(arrangement: let arr), .view(arrangement: let arr):
            id = arr.uuid
        default:
            break
        }
        model.addArrangement(type: selectedType.value, name: nameTextField.text ?? "No name", id: id).sink(receiveCompletion: { [weak self] end in
            switch end {
            case .finished: break
            case .failure(let error):
                self?.showError(errorText: error.localizedDescription)
            }
        }, receiveValue: { [weak self] arrangement in
            self?.showMessage(messageText: "\(arrangement.name ?? "Arrangement") was saved.", title: nil, completion: {
                self?.navigationController?.popViewController(animated: true)
            })
        }).store(in: &disposables)
    }

    private func delete() {
        switch mode {
        case .edit(arrangement: let arrangement), .view(arrangement: let arrangement):
            model.delete(arrangement: arrangement).sink(receiveCompletion: { [weak self] end in
                switch end {
                case .finished: break
                case .failure(let error):
                    self?.showError(errorText: error.localizedDescription)
                }
            }, receiveValue: { [weak self] _ in
                self?.navigationController?.popViewController(animated: true)
            }).store(in: &disposables)
        default: break
        }
    }

    private func fill(with arrangement: PiecesArrangement?) {
        nameTextField.text = arrangement?.name

        let type = ArrangementType(rawValue: arrangement?.arrangementType ?? ArrangementType.look.rawValue) ?? .look
        let row = allTypes.firstIndex(of: type) ?? 0

        selectedType.send(type)
        typePicker.selectRow(row, inComponent: 0, animated: false)
    }

    private func subscribeToStuff() {
        model.selectedItemsPublisher.sink(receiveValue: { [weak self] res in
            self?.collectionView.reloadData()
            self?.emptyStateLabel.isHidden = !res.isEmpty
        }).store(in: &disposables)

        selectedType.sink(receiveValue: { [weak self] arrangementType in
            self?.arrangementTypeTextField.text = arrangementType.rawValue.localized()
            switch arrangementType {
            case .capsule:
                self?.nameTextField.placeholder = "Name your capsule".localized()
            case .look:
                self?.nameTextField.placeholder = "Name your look".localized()
            }
        }).store(in: &disposables)
    }

    private func layoutCollection() {
        let count = 2
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let space = layout.minimumInteritemSpacing
        let insets = layout.sectionInset.left + layout.sectionInset.right
        cellSide = (collectionView.bounds.width - space * CGFloat(count - 1) - insets) / CGFloat(count)
        cellSide = cellSide.rounded(.down) - 16
    }

    @IBAction private func addArrangementTapped(_ sender: Any) {
        saveTapped()
    }

    @IBAction private func pickPiecesTapped(_ sender: Any) {
        performSegue(withIdentifier: "toPiecesList", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toPiecesList":
            if let vc = segue.destination as? MainVC {
                vc.depManager = dependenciesManager
                vc.purpose = .viewingPieces(dependenciesManager.mainModelViewing)
            }
        default:
            break
        }
    }
}

extension AddArrangementVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pieces.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(at: indexPath, cell: PieceCellLarge.self)
        let piece = pieces[indexPath.item]
        cell.fill(with: piece)
        return cell
    }
}

extension AddArrangementVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSide, height: cellSide)
    }
}

extension AddArrangementVC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allTypes.count
    }
}

extension AddArrangementVC: UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allTypes[row].rawValue.localized()
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedType.send(allTypes[row])
    }
}

extension AddArrangementVC: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let currentText = textField.text ?? ""

        guard let stringRange = Range(range, in: currentText) else { return false }

        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        return !allTypes.filter({$0.rawValue == updatedText}).isEmpty
    }
}
