//
//  MainModel.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 04.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation
import Combine

protocol MainModelGeneralProtocol: class {
    var addedTag: AnyPublisher<TagSignal, Never> { get }
    var removedTag: AnyPublisher<TagSignal, Never> { get }
    var displayOption: AnyPublisher<DisplayOption, Never> { get }
    var orderOption: AnyPublisher<OrderOption, Never> { get }

    var addedItem: AnyPublisher<PieceSignal, Never> { get }

    var selectedTags: [Tag] { get }
    var pieces: [Piece] { get }
    var currentDisplayoption: DisplayOption { get }
    var currentOrderOption: OrderOption { get }
    func set(displayOption: DisplayOption)
    func set(orderOption: OrderOption)
    func removeTag(_ tag: Tag)
    func displayChoice() -> Choice
    func orderChoice() -> Choice

    func fetchPiecesForSelectedTags() -> AnyPublisher<[Piece], Error>
    func fetchStuff() -> AnyPublisher<[Piece], Error>
}

protocol MainModelEditingProtocol: MainModelGeneralProtocol {
    var updatedItem: AnyPublisher<PieceSignal, Never> { get }
    var deletedItem: AnyPublisher<PieceSignal, Never> { get }
}

protocol MainModelViewingProtocol: MainModelGeneralProtocol {
    func tappedPiece(at index: Int)
    var selectedItemsPublisher: AnyPublisher<[Piece], Never> { get }
    var selectedItems: [Piece] { get set }
}

final class MainModelEditing: MainModelEditingProtocol, MainModelViewingProtocol {

    func tappedPiece(at index: Int) {
        let piece = pieces[index]
        var items = selectedItems
        if let i = selectedItems.firstIndex(of: piece) {
            items.remove(at: i)
        } else {
            items.append(piece)
        }
        selectedItems = items
    }

    var selectedItems: [Piece] {
        get {
            selectedItemsSubject.value
        }
        set {
            selectedItemsSubject.send(newValue)
        }
    }

    private let selectedItemsSubject = CurrentValueSubject<[Piece], Never>([])
    var selectedItemsPublisher: AnyPublisher<[Piece], Never> {
        selectedItemsSubject.eraseToAnyPublisher()
    }

    private let displayOptionSubject = PassthroughSubject<DisplayOption, Never>()
    var displayOption: AnyPublisher<DisplayOption, Never> {
        displayOptionSubject.eraseToAnyPublisher()
    }

    private let orderOptionSubject = PassthroughSubject<OrderOption, Never>()
    var orderOption: AnyPublisher<OrderOption, Never> {
        orderOptionSubject.eraseToAnyPublisher()
    }

    private let addedItemSubject = PassthroughSubject<PieceSignal, Never>()
    var addedItem: AnyPublisher<PieceSignal, Never> {
        addedItemSubject.eraseToAnyPublisher()
    }

    private let updatedItemSubject = PassthroughSubject<PieceSignal, Never>()
    var updatedItem: AnyPublisher<PieceSignal, Never> {
        updatedItemSubject.eraseToAnyPublisher()
    }

    private let deletedItemSubject = PassthroughSubject<PieceSignal, Never>()
    var deletedItem: AnyPublisher<PieceSignal, Never> {
        deletedItemSubject.eraseToAnyPublisher()
    }

    var addedTag: AnyPublisher<TagSignal, Never> {
        searchAndFilterService.addedTag
    }

    var removedTag: AnyPublisher<TagSignal, Never> {
        searchAndFilterService.removedTag
    }

    var selectedTags: [Tag] {
        return searchAndFilterService.selectedTags
    }

    private var piecesManager: PiecesManagerProtocol!
    private var searchAndFilterService: SearchAndFilterServiceProtocol!

    private var disposables = Set<AnyCancellable>()
    
    private(set) var pieces: [Piece] = []
    
    private(set) var currentDisplayoption: DisplayOption = .closeupCollection
    private(set) var currentOrderOption: OrderOption = .byName
    
    init(piecesManager: PiecesManagerProtocol, searchAndFilterService: SearchAndFilterServiceProtocol) {
        self.piecesManager = piecesManager
        self.searchAndFilterService = searchAndFilterService
        subscribeToStuff()
        _ = fetchStuff()
    }

    private func subscribeToStuff() {
        piecesManager.addedItemSubject.sink { [weak self] piece in
            guard let self = self else { return }
            self.pieces.append(piece)
            self.pieces = self.sortPieces(self.pieces)
            guard let index = self.pieces.firstIndex(of: piece) else { return }
            self.addedItemSubject.send(PieceSignal(piece: piece, index: index))
        }.store(in: &disposables)

        piecesManager.updatedItemSubject.sink { [weak self] piece in
            guard let index = self?.pieces.firstIndex(of: piece) else { return }
            self?.updatedItemSubject.send(PieceSignal(piece: piece, index: index))
        }.store(in: &disposables)

        piecesManager.deletedItemSubject.sink { [weak self] piece in
            guard let index = self?.pieces.firstIndex(of: piece) else { return }
            self?.pieces.remove(at: index)
            self?.deletedItemSubject.send(PieceSignal(piece: piece, index: index))
        }.store(in: &disposables)
    }
    
    func fetchStuff() -> AnyPublisher<[Piece], Error> {
        let publisher = piecesManager.fetchStuff()
        .map { [weak self] pieces -> [Piece] in
            guard let self = self else { return [] }
            self.pieces = self.sortPieces(pieces)
            return self.pieces
        }
        return publisher.eraseToAnyPublisher()
    }
    
    func fetchPiecesForSelectedTags() -> AnyPublisher<[Piece], Error> {
        let publisher = piecesManager.fetchPieces(tags: selectedTags.map { $0.idName })
            .map { [weak self] pieces -> [Piece] in
                guard let self = self else { return [] }
                self.pieces = self.sortPieces(pieces)
                return self.pieces
            }
        return publisher.eraseToAnyPublisher()
    }
    
    func removeTag(_ tag: Tag) {
        searchAndFilterService.removeTag(tag)
    }
    
    func displayChoice() -> Choice {
        let title = "Display Mode"
        let options = DisplayOption.allCases.map { Option(name: $0.rawValue, selected: currentDisplayoption == $0)}
        return Choice(kind: .display, title: title, options: options)
    }
    
    func orderChoice() -> Choice {
        let title = "Order Pieces"
        let options = OrderOption.allCases.map { Option(name: $0.rawValue, selected: currentOrderOption == $0)}
        return Choice(kind: .order, title: title, options: options)
    }
    
    func set(displayOption: DisplayOption) {
        currentDisplayoption = displayOption
        displayOptionSubject.send(displayOption)
    }
    
    func set(orderOption: OrderOption) {
        currentOrderOption = orderOption
        pieces = sortPieces(pieces)

        orderOptionSubject.send(orderOption)
    }
    
    private func sortPieces(_ pieces: [Piece]) -> [Piece] {
        switch currentOrderOption {
        case .byName:
            return pieces.sorted(by: {$0.name!.lowercased() < $1.name!.lowercased()})
        case .byNameReversed:
            return pieces.sorted(by: {$0.name!.lowercased() > $1.name!.lowercased()})
        }
    }
    
}
