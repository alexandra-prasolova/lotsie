//
//  ViewController.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 02.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import Combine

enum PiecesListPurpose {
    case editingPieces(MainModelEditingProtocol)
    case viewingPieces(MainModelViewingProtocol)
}

final class MainVC: UIViewController {
    
    @IBOutlet private var addItem: UIBarButtonItem!
    @IBOutlet private var searchItem: UIBarButtonItem!
    @IBOutlet private var searchCluesCollectionView: UICollectionView!
    @IBOutlet private var searchView: UIView!
    @IBOutlet private var emptyStateLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var displayChoiceButton: UIButton!
    @IBOutlet private var sortingChoiceButton: UIButton!

    var purpose: PiecesListPurpose!
    private var model: MainModelGeneralProtocol! {
        switch purpose! {
        case .editingPieces(let model):
            return model
        case .viewingPieces(let model):
            return model
        }
    }
    var depManager: DependenciesManager!
    
    private var pieces: [Piece] {
        return model.pieces
    }
    private var count = 2
    private var cellSide: CGFloat = 100
    private var selectedEditItemIndex: Int?

    private var disposables = Set<AnyCancellable>()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        subscribeToStuff()

        collectionView.register(PieceCellLarge.self)
        collectionView.register(PieceCellLine.self)
        collectionView.register(PieceCellSmall.self)
        updatePieces()
        
        if let layout = searchCluesCollectionView?.collectionViewLayout as? HorizontalWaterfallLayout {
            layout.delegate = self
            layout.numberOfColumns = 1
        }
        searchCluesCollectionView.register(TagCellWithDelete.self)
        
        searchOpened = false
        
        displayChoiceButton.layer.shadowColor = UIColor.black.cgColor
        displayChoiceButton.layer.shadowOffset = .zero
        sortingChoiceButton.layer.shadowColor = UIColor.black.cgColor
        sortingChoiceButton.layer.shadowOffset = .zero

        if case .viewingPieces = purpose {
            navigationItem.setRightBarButtonItems([searchItem, addItem], animated: false)
            navigationItem.leftBarButtonItem = nil
        }
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateCollectionLayout()
    }
    
    @IBAction private func addTapped(_ sender: Any) {
        performSegue(withIdentifier: "toAddVC", sender: nil)
    }
    
    @IBAction private func searchTapped(_ sender: Any) {
        performSegue(withIdentifier: "toSearchAndFilterVC", sender: nil)
    }
    
    private var searchOpened: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let self = self else { return }
                self.searchView.isHidden = !self.searchOpened
                self.searchView.alpha = self.searchOpened ? 1 : 0
            }
        }
    }
    
    private func updateCollectionLayout() {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let space = layout.minimumInteritemSpacing
        let insets = layout.sectionInset.left + layout.sectionInset.right
        cellSide = (collectionView.bounds.width - space * CGFloat(count - 1) - insets) / CGFloat(count)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func updatePieces() {
        if !model.selectedTags.isEmpty {
            model.fetchPiecesForSelectedTags().sink(receiveCompletion: { [weak self] end in
                switch end {
                case .finished: break
                case .failure(let error):
                    self?.showError(errorText: error.localizedDescription)
                }
            }, receiveValue: { [weak self] _ in
                self?.collectionView.reloadData()
                self?.emptyStateLabel.text = "Nothing matches!".localized()
            }).store(in: &disposables)
        } else {
            model.fetchStuff().sink(receiveCompletion: { [weak self] end in
                switch end {
                case .finished: break
                case .failure(let error):
                    self?.showError(errorText: error.localizedDescription)
                }
            }, receiveValue: { [weak self] _ in
                self?.collectionView.reloadData()
                self?.emptyStateLabel.text = "You don't have any records yet".localized()
            }).store(in: &disposables)
        }
        emptyStateLabel.isHidden = !pieces.isEmpty
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toAddVC":
            let vc = segue.destination as! AddVC
            vc.piecesManager = depManager.piecesManager
            vc.purpose = .add
        case "toDetailsVC":
            guard let i = selectedEditItemIndex else { return }
            let vc = segue.destination as! DetailsVC
            vc.piecesManager = depManager.piecesManager
            vc.piece = pieces[i]
            selectedEditItemIndex = nil
        case "toSearchAndFilterVC":
            let vc = segue.destination as! SearchAndFilterVC
            vc.searchAndFilterService = depManager.searchAndFilterService
        case "toSettings":
            break
        case "toSortingChoice":
            let vc = segue.destination as! OptionsVC
            vc.choiceKind = .order
            vc.mainModel = model
        case "toDisplayChoice":
            let vc = segue.destination as! OptionsVC
            vc.choiceKind = .display
            vc.mainModel = model
        default:
            break
        }
    }
    
}

// MARK: UICollectionViewDataSource

extension MainVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case searchCluesCollectionView:
            return model.selectedTags.count
        case collectionView:
            return pieces.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView:
            let cell: PieceCell
            switch model.currentDisplayoption {
            case .closeupCollection:
                cell = collectionView.dequeue(at: indexPath, cell: PieceCellLarge.self)
            case .collection:
                cell = collectionView.dequeue(at: indexPath, cell: PieceCellSmall.self)
            case .list:
                cell = collectionView.dequeue(at: indexPath, cell: PieceCellLine.self)
                (cell as! PieceCellLine).set(number: indexPath.row+1)
            }
            cell.fill(with: pieces[indexPath.item])
            switch purpose! {
            case .viewingPieces(let model):
                cell.set(checked: model.selectedItems.contains(pieces[indexPath.item]))
            default: cell.set(checked: false)
            }
            return cell
        case searchCluesCollectionView:
            let cell = searchCluesCollectionView.dequeue(at: indexPath, cell: TagCellWithDelete.self)
            let tag = model.selectedTags[indexPath.item]
            cell.set(name: tag.localizedName)
            cell.deleteBlock = { [weak self] in
                self?.model.removeTag(tag)
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension MainVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch model.currentDisplayoption {
        case .closeupCollection, .collection:
            return CGSize(width: cellSide, height: cellSide)
        case .list:
            return CGSize(width: cellSide, height: 44)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.collectionView:
            switch purpose! {
            case .editingPieces:
                selectedEditItemIndex = indexPath.item
                performSegue(withIdentifier: "toDetailsVC", sender: nil)
            case .viewingPieces(let model):
                model.tappedPiece(at: indexPath.item)
                collectionView.reloadItems(at: [indexPath])
            }
        default:
            break
        }
    }
    
}

// MARK: MainModelObserver

extension MainVC {

    func subscribeToStuff() {
        model.displayOption.sink { [weak self] displayOption in
            switch displayOption {
            case .closeupCollection:
                self?.count = 2
                self?.displayChoiceButton.setImage(UIImage(systemName: "square.grid.2x2.fill"), for: .normal)
            case .collection:
                self?.count = 4
                self?.displayChoiceButton.setImage(UIImage(systemName: "square.grid.4x3.fill"), for: .normal)
            case .list:
                self?.count = 1
                self?.displayChoiceButton.setImage(UIImage(systemName: "list.dash"), for: .normal)
            }
            self?.updateCollectionLayout()
            self?.collectionView.reloadData()
        }.store(in: &disposables)

        model.orderOption.sink { [weak self] _ in
            self?.collectionView.reloadData()
        }.store(in: &disposables)

        model.addedItem.sink { [weak self] pieceSignal in
            guard let self = self else { return }
            let indexPath = IndexPath(item: pieceSignal.index, section: 0)
            self.collectionView.insertItems(at: [indexPath])
            self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            self.emptyStateLabel.isHidden = !self.pieces.isEmpty
        }.store(in: &disposables)

        switch purpose {
        case .editingPieces(let model):
            model.updatedItem.sink { [weak self] pieceSignal in
                self?.collectionView.reloadItems(at: [IndexPath(item: pieceSignal.index, section: 0)])
            }.store(in: &disposables)

            model.deletedItem.sink { [weak self] pieceSignal in
                guard let self = self else { return }
                let indexPath = IndexPath(item: pieceSignal.index, section: 0)
                self.collectionView.deleteItems(at: [indexPath])
                self.emptyStateLabel.isHidden = !self.pieces.isEmpty
            }.store(in: &disposables)
        case .viewingPieces:
            break
        case .none:
            break
        }

        model.addedTag.sink { [weak self] tagSignal in
            guard let self = self else { return }
            self.searchOpened = !self.model.selectedTags.isEmpty
            self.updatePieces()
            self.searchCluesCollectionView.insertItems(at: [IndexPath(item: tagSignal.index, section: 0)])
        }.store(in: &disposables)

        model.removedTag.sink { [weak self] tagSignal in
            guard let self = self else { return }
            self.searchOpened = !self.model.selectedTags.isEmpty
            self.searchCluesCollectionView.deleteItems(at: [IndexPath(item: tagSignal.index, section: 0)])
            self.updatePieces()
        }.store(in: &disposables)
    }
}

//MARK: HorizontalWaterfallLayoutDelegate

extension MainVC: HorizontalWaterfallLayoutDelegate {
    
    func collectionViewLayout(_ layout: HorizontalWaterfallLayout, widthForCellAtIndexPath indexPath: IndexPath) -> CGFloat {
        return TagCellWithDelete.width(for: model.selectedTags[indexPath.item].localizedName)
    }
    
    func collectionViewLayoutWidthForHeader(_ layout: HorizontalWaterfallLayout) -> CGFloat {
        return 14
    }
    
    func collectionViewLayoutWidthForFooter(_ layout: HorizontalWaterfallLayout) -> CGFloat {
        return 14
    }
    
}
