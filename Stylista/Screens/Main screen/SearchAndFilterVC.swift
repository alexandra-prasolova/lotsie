//
//  SearchAndFilterVC.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 22.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import Combine

final class SearchAndFilterVC: UIViewController {
    
    var searchAndFilterService: SearchAndFilterServiceProtocol!
    
    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var collectionView: UICollectionView!
    
    private var allTags = Tag.allTags()

    private var disposables = Set<AnyCancellable>()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.searchTextField.leftView?.tintColor = .tintColor
        collectionView.register(TagCellWithDelete.self)
        
        if let layout = collectionView?.collectionViewLayout as? HorizontalWaterfallLayout {
            layout.delegate = self
            layout.numberOfColumns = 1
        }
        
        tableView.register(SearchResultCell.self)
        tableView.tableFooterView = UIView()
        
        collectionView.isHidden = searchAndFilterService.selectedTags.isEmpty

        setupBindings()
    }

    private func setupBindings() {
        searchAndFilterService.addedTag.sink(receiveValue: { [weak self] tagSignal in
            let tag = tagSignal.tag
            let index = tagSignal.index
            self?.collectionView.insertItems(at: [IndexPath(item: index, section: 0)])
            if let i = self?.allTags.firstIndex(of: tag) {
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            }
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.collectionView.isHidden = self?.searchAndFilterService.selectedTags.isEmpty ?? false
            })
        }).store(in: &disposables)

        searchAndFilterService.removedTag.sink { [weak self] tagSignal in
            let tag = tagSignal.tag
            let index = tagSignal.index
            self?.collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
            if let i = self?.allTags.firstIndex(of: tag) {
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            }
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.collectionView.isHidden = self?.searchAndFilterService.selectedTags.isEmpty ?? false
            })
        }.store(in: &disposables)
    }

    @IBAction private func showResultsTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: UICollectionViewDataSource

extension SearchAndFilterVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchAndFilterService.selectedTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(at: indexPath, cell: TagCellWithDelete.self)
        let tag = searchAndFilterService.selectedTags[indexPath.item]
        cell.set(name: tag.localizedName)
        cell.deleteBlock = { [weak self] in
            self?.searchAndFilterService.removeTag(tag)
        }
        return cell
    }
    
}

// MARK: HorizontalWaterfallLayoutDelegate

extension SearchAndFilterVC: HorizontalWaterfallLayoutDelegate {
    
    func collectionViewLayout(_ layout: HorizontalWaterfallLayout, widthForCellAtIndexPath indexPath: IndexPath) -> CGFloat {
        return TagCellWithDelete.width(for: searchAndFilterService.selectedTags[indexPath.item].localizedName)
    }
    
    func collectionViewLayoutWidthForHeader(_ layout: HorizontalWaterfallLayout) -> CGFloat {
        return 14
    }
    
    func collectionViewLayoutWidthForFooter(_ layout: HorizontalWaterfallLayout) -> CGFloat {
        return 14
    }
    
}

//MARK: UITableViewDataSource

extension SearchAndFilterVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(at: indexPath, cell: SearchResultCell.self)
        let checked = searchAndFilterService.selectedTags.contains(allTags[indexPath.row])
        cell.set(title: allTags[indexPath.row].localizedName, checked: checked)
        return cell
    }
}

//MARK: UITableViewDelegate

extension SearchAndFilterVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !searchAndFilterService.selectedTags.contains(allTags[indexPath.row]) else { return }
        searchAndFilterService.addTag(allTags[indexPath.row])
        collectionView.scrollToItem(at: IndexPath(item: searchAndFilterService.selectedTags.count-1, section: 0), at: .right, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
}

//MARK: UISearchBarDelegate

extension SearchAndFilterVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            allTags = Tag.allTags()
        } else {
            allTags = Tag.allTags().filter { $0.localizedName.uppercased().contains(searchText.uppercased()) }
        }
        tableView.reloadData()
    }
    
}
