//
//  OptionCell.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 04.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class OptionCell: UITableViewCell {
    
    static var height: CGFloat {
        return "A".height(width: 40, font: UIFont.preferredFont(forTextStyle: .body)) + 24
    }

    @IBOutlet private var optionLabel: UILabel!
    @IBOutlet private var tickImageView: UIImageView!
    
    func set(option: String, selected: Bool) {
        optionLabel.text = option
        tickImageView.isHidden = !selected
    }
    
}
