//
//  AddVC.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 02.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import AVFoundation
import Combine

final class AddVC: UIViewController {
    
    enum Purpose {
        case add
        case edit(piece: Piece)
    }
    
    var piecesManager: PiecesManagerProtocol!
    var purpose: Purpose = .add

    @IBOutlet private var buttonsStackView: UIStackView!
    @IBOutlet private var textField: UITextField!
    @IBOutlet private var imageViewContainer: UIView!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var tagsCollectionHeight: NSLayoutConstraint!
    @IBOutlet private var buttons: [UIButton]!
    
    private var imageView = ScrollableImageView.loadFromNib()
    private var tags: [Tag] = Tag.allTags()
    private var selectedIndices = [IndexPath]()

    private var disposables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageViewContainer.addSubview(imageView)
        startWatchingTapOutsideTextInput()
        
        if let layout = collectionView?.collectionViewLayout as? HorizontalWaterfallLayout {
            layout.delegate = self
            layout.numberOfColumns = UIScreen.main.bounds.height > 700 ? 3 : 2
            
        }
        tagsCollectionHeight.constant = UIScreen.main.bounds.height > 700 ? 150 : 100
        collectionView.register(TagCell.self)
        
        buttons.forEach() {
            $0.layer.cornerRadius = 5
            $0.layer.borderWidth = 2
            $0.layer.borderColor = UIColor.secondaryBackground.cgColor
        }
        imageViewContainer.layer.cornerRadius = 5
        
        switch purpose {
        case .edit(piece: let piece):
            textField.text = piece.name
            set(piece.image)
            let pieceTags: [Tag] = (piece.labels?.map { Tag($0) } ?? [])
            for tag in pieceTags {
                if let i = tags.firstIndex(where: {tag == $0}) {
                    selectedIndices.append(IndexPath(item: i, section: 0))
                }
            }
        default:
            break
        }
        
        let contextMenuInteraction = UIContextMenuInteraction(delegate: self)
        imageView.addInteraction(contextMenuInteraction)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageView.frame = imageViewContainer.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.allowsSelection = true
        collectionView.allowsMultipleSelection = true
    }
    
    private func set(_ image: UIImage?) {
        imageView.image = image
        buttonsStackView.isHidden = image != nil
    }
    
    @IBAction private func saveTapped(_ sender: Any) {
        guard let img = imageView.image else {
            showError(errorText: "You can't save a piece without any image".localized())
            return
        }
        guard let title = textField.text, !title.isEmpty else {
            showError(errorText: "You can't save a piece without any title".localized())
            return
        }
        let tagsIds = self.selectedIndices.map { tags[$0.item].idName }
        
        switch purpose {
        case .add:
            piecesManager.save(name: textField.text!, image: img, tags: tagsIds).sink(receiveCompletion: { [weak self] end in
                switch end {
                case .finished: break
                case .failure(let error):
                    self?.showError(errorText: error.localizedDescription)
                }
            }, receiveValue: { [weak self] success in
                if success {
                    self?.showMessage(messageText: "Your piece is saved.".localized(), title: "Yupp!".localized()) {
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }).store(in: &disposables)

        case .edit(piece: let piece):
            piecesManager.update(piece: piece, withName: textField.text!, image: img, tags: tagsIds).sink(receiveCompletion: { [weak self] end in
                switch end {
                case .finished: break
                case .failure(let error):
                    self?.showError(errorText: error.localizedDescription)
                }
            }, receiveValue: { [weak self] success in
                self?.showMessage(messageText: "Your piece was updated.".localized(), title: "Great!".localized()) {
                    self?.navigationController?.popViewController(animated: true)
                }
            }).store(in: &disposables)
        }
    }
    
    @IBAction private func libraryTapped(_ sender: Any) {
        let imagePickerVC = UIImagePickerController.forLibrary
        imagePickerVC.delegate = self
        present(imagePickerVC, animated: true)
    }
    
    @IBAction private func cameraTapped(_ sender: Any) {
        checkCameraPermission(
        grantedBlock:
        { [weak self] in
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else { return }
            let imagePickerVC = UIImagePickerController.forPhoto
            imagePickerVC.delegate = self
            self?.present(imagePickerVC, animated: true)
        },
        deniedBlock: { [weak self] in
            self?.prompt(title: "Go to Settings?".localized(), subtitle: "Need your permission to access camera".localized(), actionTitle: "Settings".localized(), acceptedBlock: {
                 UIApplication.openSettings()
            })
        })
    }
    
    private  func checkCameraPermission(grantedBlock: @escaping EmptyBlock, deniedBlock: @escaping EmptyBlock) {
           let access = AVCaptureDevice.authorizationStatus(for: .video)
           
           switch access {
           case .authorized:
               DispatchQueue.main.async {
                   grantedBlock()
               }
           case .notDetermined:
               AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                   if granted {
                       DispatchQueue.main.async {
                           grantedBlock()
                       }
                   }
               })
           case .denied, .restricted:
               DispatchQueue.main.async {
                   deniedBlock()
               }
           @unknown default:
               break
           }
       }
    
}

// MARK: Image picker delegate

extension AddVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else { return }
        set(image)
    }
    
}

// MARK: CollectionView

extension AddVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeue(at: indexPath, cell: TagCell.self)
        cell.set(name: tags[indexPath.item].localizedName, selected: selectedIndices.contains(indexPath))
        cell.tapBlock = { [weak self] in
            if let i = self?.selectedIndices.firstIndex(of: indexPath) {
                self?.selectedIndices.remove(at: i)
            } else {
                self?.selectedIndices.append(indexPath)
            }
            self?.collectionView.reloadItems(at: [indexPath])
        }
        return cell
    }
}

// MARK: - CollectionView waterfall layout

extension AddVC: HorizontalWaterfallLayoutDelegate {
    
    func collectionViewLayout(_ layout: HorizontalWaterfallLayout, widthForCellAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        return TagCell.width(for: tags[indexPath.item].localizedName)
    }
    
    func collectionViewLayoutWidthForHeader(_ layout: HorizontalWaterfallLayout) -> CGFloat {
        return 17
    }
    
    func collectionViewLayoutWidthForFooter(_ layout: HorizontalWaterfallLayout) -> CGFloat {
        return 17
    }
    
}

//MARK: - UIContextMenuInteractionDelegate

extension AddVC: UIContextMenuInteractionDelegate {
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        
        guard imageView.image != nil else { return nil }
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { actions -> UIMenu? in
            
            let actionClearAll = UIAction(title: NSLocalizedString("Clear Image".localized(), comment: ""), image: UIImage(systemName: "trash"), attributes: [UIMenuElement.Attributes.destructive]) { [weak self] _ in
                self?.prompt(title: NSLocalizedString("Clear this image?".localized(), comment: ""), subtitle: nil, actionTitle: NSLocalizedString("Clear".localized(), comment: ""), destructive: true, acceptedBlock: { [weak self] in
                    self?.imageView.image = nil
                    self?.buttonsStackView.isHidden = false
                })
            }
            
            let actions: [UIAction] = [actionClearAll]
            
            return UIMenu(title: NSLocalizedString("Actions".localized(), comment: ""), image: nil, identifier: nil, options: [], children: actions)
            
        })
    }
    
}
