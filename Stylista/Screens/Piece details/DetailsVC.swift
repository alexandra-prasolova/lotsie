//
//  DetailsVC.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 02.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import Combine

final class DetailsVC: UIViewController {
    
    var piece: Piece! {
        didSet {
            nameLabel?.text = piece.name
            imageView?.image = piece.image
            tags = (piece.labels?.map { Tag($0) } ?? [] )
            orderTagsForDisplay()
            collectionView?.reloadData()
        }
    }
    
    var piecesManager: PiecesManagerProtocol!
    
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    
    private var tags = [Tag]()
    
    private var orderedTags = [[Tag]()]

    private var disposables = Set<AnyCancellable>()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        piecesManager.updatedItemSubject.sink(receiveValue: { [weak self] piece in
            if self?.piece.uuid == piece.uuid {
                self?.piece = piece
            }
        }).store(in: &disposables)

        imageView.image = piece?.image
        tags = (piece?.labels?.map { Tag($0) } ?? [] )
        tags.sort(by: { $0.localizedName.count > $1.localizedName.count })
        nameLabel.text = piece?.name
        
        collectionView.register(TagCell.self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        orderTagsForDisplay()
        collectionView.reloadData()
    }
    
    private func orderTagsForDisplay() {
        guard collectionView != nil else { return }
        let colletcionWidth = collectionView.bounds.width
        orderedTags = [[Tag]()]
        
        for i in 0..<tags.count {
            guard i < tags.count else { break }
            let tag = tags[i]
            
            let width = orderedTags.last?.reduce(0, { accum, t in
                return accum + cellWidth(for: t) + 8
            }) ?? 0
            let tagWidth = cellWidth(for: tag)
            if tagWidth + width > colletcionWidth {
                if let (fittingTag, j) = maybeTheresFittingTag(spaceLeft: colletcionWidth - width, lastIndex: i) {
                    tags.remove(at: j)
                    orderedTags[orderedTags.count-1].append(fittingTag)
                }
                orderedTags.append([tag])
            } else {
                orderedTags[orderedTags.count-1].append(tag)
            }
        }
        if orderedTags.count > 2 {
            var tagsToShuffle: [[Tag]] = Array(orderedTags[0...(orderedTags.count - 2)])
            let tagsNotToShuffle: [[Tag]] = [orderedTags[orderedTags.count - 1]]
            tagsToShuffle.shuffle()
            orderedTags = tagsToShuffle + Array(tagsNotToShuffle)
        }
    }
    
    private func maybeTheresFittingTag(spaceLeft: CGFloat, lastIndex: Int) -> (Tag, Int)? {
        guard lastIndex < tags.count else { return nil }
        for i in lastIndex+1..<tags.count {
            if cellWidth(for: tags[i]) < spaceLeft {
                let tag = tags[i]
                return (tag, i)
            }
        }
        return nil
    }
    
    private func cellWidth(for tag: Tag) -> CGFloat {
        return TagCell.width(for: tag.localizedName)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toEdit":
            guard let vc = segue.destination as? AddVC else { break }
            vc.purpose = .edit(piece: piece)
            vc.piecesManager = piecesManager
        default:
            break
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        let deletion = { [weak self] in
            guard let self = self else { return }
            self.piecesManager.delete(piece: self.piece).sink(receiveCompletion: { end in
                switch end {
                case .finished:
                    break
                case .failure(let error):
                    self.showError(errorText: error.localizedDescription)
                }
            }, receiveValue: { [weak self] _ in
                self?.navigationController?.popViewController(animated: true)
            }).store(in: &self.disposables)
        }
        prompt(title: "Are you sure you want to delete this piece?".localized(), subtitle: nil, actionTitle: "Yes".localized(), acceptedBlock: deletion)
    }
}

// MARK: CollectionView

extension DetailsVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return orderedTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orderedTags[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeue(at: indexPath, cell: TagCell.self)
        cell.set(name: orderedTags[indexPath.section][indexPath.item].localizedName, selected: false)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = cellWidth(for: orderedTags[indexPath.section][indexPath.item])
        return CGSize(width: width, height: 42)
    }
}
