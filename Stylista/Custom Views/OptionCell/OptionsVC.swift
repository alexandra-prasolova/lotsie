//
//  OptionsVC.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 04.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class OptionsVC: UIViewController {
    
    var mainModel: MainModelGeneralProtocol!
    var choiceKind: Choice.Kind = .display
    
    @IBOutlet private var tableViewHeight: NSLayoutConstraint!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var titleLabel: UILabel!
    
    private var options: [Option] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        let choice: Choice
        switch choiceKind {
        case .display:
            choice = mainModel.displayChoice()
        case .order:
            choice = mainModel.orderChoice()
        }
        
        options = choice.options
        titleLabel.text = choice.title.localized()
        
        tableView.register(OptionCell.self)
        tableViewHeight.constant = OptionCell.height * CGFloat(options.count)
        tableView.reloadData()
    }
    
    @IBAction private func closeBuuttonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension OptionsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return OptionCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (i, _) in options.enumerated() {
            options[i].selected = false
        }
        options[indexPath.row].selected = true
        tableView.reloadData()
        
        switch choiceKind {
        case .display:
            mainModel.set(displayOption: DisplayOption(rawValue: options[indexPath.row].name)!)
        case .order:
            mainModel.set(orderOption: OrderOption(rawValue: options[indexPath.row].name)!)
        }
        
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}

extension OptionsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(at: indexPath, cell: OptionCell.self)
        let option = options[indexPath.row]
        cell.set(option: option.name.localized(), selected: option.selected)
        
        return cell
    }
    
}
