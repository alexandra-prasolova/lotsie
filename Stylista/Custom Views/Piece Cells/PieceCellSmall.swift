//
//  PieceCellSmall.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 05.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class PieceCellSmall: PieceCell {

    @IBOutlet private var pieceImageview: UIImageView!
    @IBOutlet private var checkmark: UIImageView!

    override func fill(with piece: Piece) {
        pieceImageview.image = piece.image
    }

    override func set(checked: Bool) {
        checkmark.isHidden = !checked
    }

}
