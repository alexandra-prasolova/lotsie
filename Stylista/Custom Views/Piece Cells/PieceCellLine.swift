//
//  PieceCellLine.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 05.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class PieceCellLine: PieceCell {
    
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var pieceImageView: UIImageView!
    @IBOutlet private var checkmark: UIImageView!

    override func fill(with piece: Piece) {
        titleLabel.text = piece.name
        pieceImageView.image = piece.image
    }

    func set(number: Int) {
        numberLabel.text = "\(number)"
    }

    override func set(checked: Bool) {
        checkmark.isHidden = !checked
    }
}
