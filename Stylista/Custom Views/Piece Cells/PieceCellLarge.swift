//
//  CollectionViewCell.swift
//  Shkaf
//
//  Created by Alexandra Prasolova on 02.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

// This class is meant to be generic, DO NOT USE except for inheriting
class PieceCell: UICollectionViewCell {
    func fill(with piece: Piece) {}
    func set(checked: Bool) {}
}

final class PieceCellLarge: PieceCell {
    
    @IBOutlet private var pieceImageView: UIImageView!
    @IBOutlet private var checkmark: UIImageView!
    @IBOutlet private var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        checkmark.isHidden = true
    }
    
    override func fill(with piece: Piece) {
        pieceImageView.image = piece.image
        nameLabel.text = piece.name
    }

    override func set(checked: Bool) {
        checkmark.isHidden = !checked
    }
    
}
