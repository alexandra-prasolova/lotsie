//
//  SearchResultCell.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 23.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class SearchResultCell: UITableViewCell {
    
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var ceckmark: UIImageView!
    
    func set(title: String, checked: Bool) {
        nameLabel.text = title
        ceckmark.isHidden = !checked
    }
    
}
