//
//  HorizontalWaterfallLayout.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 15.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

@objc protocol HorizontalWaterfallLayoutDelegate: class {
    func collectionViewLayout(_ layout: HorizontalWaterfallLayout, widthForCellAtIndexPath indexPath: IndexPath) -> CGFloat
    @objc optional func collectionViewLayoutWidthForHeader(_ layout: HorizontalWaterfallLayout) -> CGFloat
    @objc optional func collectionViewLayoutWidthForFooter(_ layout: HorizontalWaterfallLayout) -> CGFloat
}

final class HorizontalWaterfallLayout: UICollectionViewLayout {
    
    weak var delegate: HorizontalWaterfallLayoutDelegate!
    var numberOfColumns = 2
    var cellPadding: CGFloat = 8
    var vertical = false
    
    private(set) var columnHeigh: CGFloat = 0
    
    // Array to keep a cache of attributes.
    private var cache = [UICollectionViewLayoutAttributes]()
    private var headerAttributes: UICollectionViewLayoutAttributes?
    
    private var contentWidth: CGFloat = 0
    private var contentHeight: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.height - (insets.bottom + insets.top)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth + (delegate.collectionViewLayoutWidthForFooter?(self) ?? 0), height: contentHeight)
    }
    
    override func prepare() {
       
        guard let collectionView = collectionView else { return }
        
        if let width = delegate.collectionViewLayoutWidthForHeader?(self) {
            let frame = CGRect(x: 0, y: 0, width: width, height: contentHeight)
            let headerIndexPath = IndexPath(item: 0, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: headerIndexPath)
            attributes.frame = frame
            headerAttributes = attributes
        }

        cache = []
        // Pre-Calculates the X Offset for every column and adds an array to increment the currently max Y Offset for each column
        columnHeigh = (contentHeight - cellPadding * CGFloat(numberOfColumns - 1)) / CGFloat(numberOfColumns)
        var yOffsets = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            yOffsets.append(CGFloat(column) * (columnHeigh + cellPadding))
        }
        var column = 0
        var xOffset = [CGFloat](repeating: (headerAttributes?.frame.maxX ?? 0), count: numberOfColumns)
        
        // Iterates through the list of items in the first section
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
            
            let indexPath = IndexPath(item: item, section: 0)
            
            // Asks the delegate for the width of the cell and the annotation and calculates the cell frame.
            let width = max(columnHeigh, delegate.collectionViewLayout(self, widthForCellAtIndexPath: indexPath)) + cellPadding
            let frame = CGRect(x: xOffset[column], y:yOffsets[column] , width: width, height: columnHeigh)
            let insetFrame = frame.insetBy(dx: cellPadding/2, dy: 0)
            
            // Creates an UICollectionViewLayoutItem with the frame and add it to the cache
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            
            // Updates the collection view content width
            contentWidth = max(contentWidth, frame.maxX)
            xOffset[column] = xOffset[column] + width
            
            column = column < (numberOfColumns - 1) ? (column + 1) : 0
        }
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if elementKind == UICollectionView.elementKindSectionHeader {
            return headerAttributes
        }
        return nil
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        if let headerAttributes = headerAttributes, headerAttributes.frame.intersects(rect) {
            visibleLayoutAttributes.append(headerAttributes)
        }
        
        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
}

