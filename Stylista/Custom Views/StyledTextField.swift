//
//  StyledTextField.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 16.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class StyledTextField: UITextField {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        let placeholderText = NSAttributedString(string: placeholder ?? "",
                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.secondaryBackground])

        attributedPlaceholder = placeholderText
        layer.borderWidth = 1
        layer.borderColor = UIColor.secondaryBackground.cgColor
        clipsToBounds = true

        backgroundColor = .background
        borderStyle = .roundedRect
        layer.cornerRadius = 4
        tintColor = .tintColor
        textColor = .textColor
    }
}
