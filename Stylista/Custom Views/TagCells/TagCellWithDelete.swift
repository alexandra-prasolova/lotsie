//
//  TagCellWithDelete.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 22.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class TagCellWithDelete: UICollectionViewCell {
    
    static func width(for string: String) -> CGFloat {
        return string.width(height: 30, font: TagCellWithDelete.titleFont) + recommendedHorizontalMargin
    }
    
    private static let titleFont = UIFont.preferredFont(forTextStyle: .body)
    private static let recommendedHorizontalMargin: CGFloat = 57
    
    var deleteBlock: EmptyBlock?
    
    @IBOutlet private var tagLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = contentView.frame.height/2
    }
    
    func set(name: String) {
        tagLabel.text = name
    }
    
    @IBAction private func tappedDelete(_ sender: Any) {
        deleteBlock?()
    }
    
}
