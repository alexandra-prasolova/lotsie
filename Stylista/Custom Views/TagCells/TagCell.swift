//
//  TagCell.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 15.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class TagCell: UICollectionViewCell {
    
    static func width(for string: String) -> CGFloat {
        return string.width(height: 30, font: TagCell.titleFont) + recommendedHorizontalMargin
    }
    
    private static let titleFont = UIFont.preferredFont(forTextStyle: .body)
    private static let recommendedHorizontalMargin: CGFloat = 16
    
    var tapBlock: EmptyBlock?
    
    @IBOutlet private var tagLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = contentView.frame.height/2
    }
    
    func set(name: String, selected: Bool) {
        contentView.backgroundColor = selected ? .tintColor : .viewBackground
        tagLabel.text = name
    }
    
    @IBAction private func tappedCell(_ sender: Any) {
        tapBlock?()
    }
    
}
