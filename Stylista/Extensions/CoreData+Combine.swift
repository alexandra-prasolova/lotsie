//
//  CoreData+Combine.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 08.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Combine
import CoreData

typealias Action = EmptyBlock

// MARK: - Save
struct CoreDataSaveModelPublisher: Publisher {
    typealias Output = Bool
    typealias Failure = Error

    private let action: Action
    private let context: NSManagedObjectContext

    init(afterSaveAction: @escaping Action, context: NSManagedObjectContext) {
        self.action = afterSaveAction
        self.context = context
    }

    func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        let subscription = Subscription(subscriber: subscriber, context: context, action: action)
        subscriber.receive(subscription: subscription)
    }
}

extension CoreDataSaveModelPublisher {
    class Subscription<S> where S : Subscriber, Failure == S.Failure, Output == S.Input {
        private var subscriber: S?
        private let afterSaveAction: Action
        private let context: NSManagedObjectContext

        init(subscriber: S, context: NSManagedObjectContext, action: @escaping Action) {
            self.subscriber = subscriber
            self.context = context
            self.afterSaveAction = action
        }
    }
}

extension CoreDataSaveModelPublisher.Subscription: Subscription {
    func request(_ demand: Subscribers.Demand) {
        var demand = demand
        guard let subscriber = subscriber, demand > 0 else { return }

        do {
            demand -= 1
            try context.save()
            afterSaveAction()
            demand += subscriber.receive(true)
        } catch {
            subscriber.receive(completion: .failure(error))
        }
    }
}

extension CoreDataSaveModelPublisher.Subscription: Cancellable {
    func cancel() {
        subscriber = nil
    }
}

// MARK: Fetch

struct CoreDataFetchResultsPublisher<Entity>: Publisher where Entity: NSManagedObject {
    typealias Output = [Entity]
    typealias Failure = Error

    private let action: Action
    private let request: NSFetchRequest<Entity>
    private let context: NSManagedObjectContext

    init(request: NSFetchRequest<Entity>, context: NSManagedObjectContext, action: @escaping Action) {
        self.request = request
        self.context = context
        self.action = action
    }

    func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        let subscription = Subscription(subscriber: subscriber, context: context, request: request, action: action)
        subscriber.receive(subscription: subscription)
    }
}

extension CoreDataFetchResultsPublisher {
    class Subscription<S> where S : Subscriber, Failure == S.Failure, Output == S.Input {
        private var subscriber: S?
        private var request: NSFetchRequest<Entity>
        private var context: NSManagedObjectContext
        private let action: Action

        init(subscriber: S, context: NSManagedObjectContext, request: NSFetchRequest<Entity>, action: @escaping Action) {
            self.subscriber = subscriber
            self.context = context
            self.request = request
            self.action = action
        }
    }
}

extension CoreDataFetchResultsPublisher.Subscription: Subscription {
    func request(_ demand: Subscribers.Demand) {
        var demand = demand
        guard let subscriber = subscriber, demand > 0 else { return }
        do {
            demand -= 1
            let items = try context.fetch(request)
            action()
            demand += subscriber.receive(items)
        } catch {
            subscriber.receive(completion: .failure(error))
        }
    }
}

extension CoreDataFetchResultsPublisher.Subscription: Cancellable {
    func cancel() {
        subscriber = nil
    }
}

// MARK: Delete

struct CoreDataDeleteModelPublisher: Publisher {
    typealias Output = Bool
    typealias Failure = Error

    private let action: Action
    private let request: NSFetchRequest<NSFetchRequestResult>
    private let context: NSManagedObjectContext

    init(delete request: NSFetchRequest<NSFetchRequestResult>, context: NSManagedObjectContext, afterDeleteAction: @escaping Action) {
        self.request = request
        self.context = context
        self.action = afterDeleteAction
    }

    func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        let subscription = Subscription(subscriber: subscriber, context: context, request: request, action: action)
        subscriber.receive(subscription: subscription)
    }
}

extension CoreDataDeleteModelPublisher {
    class Subscription<S> where S : Subscriber, Failure == S.Failure, Output == S.Input {
        private var subscriber: S?
        private let request: NSFetchRequest<NSFetchRequestResult>
        private var context: NSManagedObjectContext
        private let action: Action

        init(subscriber: S, context: NSManagedObjectContext, request: NSFetchRequest<NSFetchRequestResult>, action: @escaping Action) {
            self.subscriber = subscriber
            self.context = context
            self.request = request
            self.action = action
        }
    }
}

extension CoreDataDeleteModelPublisher.Subscription: Subscription  {
    func request(_ demand: Subscribers.Demand) {
        var demand = demand
        guard let subscriber = subscriber, demand > 0 else { return }

        do {
            demand -= 1
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: self.request)
            batchDeleteRequest.resultType = .resultTypeObjectIDs

            if let result = try context.execute(batchDeleteRequest) as? NSBatchDeleteResult {
                let changes: [AnyHashable: Any] = [NSDeletedObjectsKey: result.result as! [NSManagedObjectID]]
                NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [context])
                demand += subscriber.receive(true)
                action()
            }
            else {
                subscriber.receive(completion: .failure(NSError()))
            }

        } catch {
            subscriber.receive(completion: .failure(error))
        }
    }
}

extension CoreDataDeleteModelPublisher.Subscription: Cancellable {
    func cancel() {
        subscriber = nil
    }
}
