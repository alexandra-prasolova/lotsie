//
//  NavigationController.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 17.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {

    var dependenciesManager: DependenciesManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.shadowImage = UIImage()
    }
    
    override var childForStatusBarStyle: UIViewController? {
        return topViewController
    }

    func setDependencies() {
        guard let vc = viewControllers.first else { return }

        if let main = vc as? MainVC {
            main.depManager = dependenciesManager
            main.purpose = .editingPieces(dependenciesManager.mainModelEditing)
            tabBarItem.title = "All things".localized()
        }

        if let _ = vc as? SettingsVC {
            tabBarItem.title = "Settings".localized()
        }

        if let arrangements = vc as? LooksAndCapsulesVC {
            tabBarItem.title = "Looks and Capsules".localized()
            arrangements.dependenciesManager = dependenciesManager
            arrangements.arrangementsListProvider = dependenciesManager.looksAndCapsulesListProvider
        }
    }
}
