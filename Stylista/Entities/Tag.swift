//
//  Tag.swift
//  Stylista
//
//  Created by Alexandra Prasolova on 20.06.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

struct Tag: Equatable {
    var idName: String
    var localizedName: String {
        return idName.localized()
    }
    init(_ idName: String) {
        self.idName = idName
    }
    
    static func allTags() -> [Tag] {
        [Tag("Top"), Tag("Bottom"), Tag("Dress"), Tag("Shirt"), Tag("Summer"), Tag("Casual"), Tag("Daytime"), Tag("Nighttime"), Tag("Spring"), Tag("Winter"), Tag("Cotton"), Tag("Wool"), Tag("Sport"), Tag("Funny"), Tag("Jeans"), Tag("T-shirt"), Tag("Bodysuit"), Tag("Formal"), Tag("Working"), Tag("Hat"), Tag("Gloves"), Tag("Skirt"), Tag("Shorts"), Tag("Trouser"), Tag("Long"), Tag("Midi"), Tag("Mini"), Tag("Jumpsuit"), Tag("Underwear"), Tag("Cardigans&blazers"), Tag("Coat"), Tag("Oversize"), Tag("Tight"), Tag("Longsleeve"), Tag("Sleeveless"), Tag("Shortsleeve"), Tag("Cropped"), Tag("Homewear"), Tag("Shoes")]
    }
}
