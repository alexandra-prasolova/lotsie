//
//  PieceSignal.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 10.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Foundation

struct PieceSignal {
    var piece: Piece
    var index: Int
}
