//
//  PiecesArrangementExtension.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 20.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Foundation

extension PiecesArrangement {
    var piecesArray: [Piece] {
        return pieces?.allObjects as? [Piece] ?? []
    }
}
