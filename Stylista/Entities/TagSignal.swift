//
//  TagSignal.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 10.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Foundation

struct TagSignal {
    var tag: Tag
    var index: Int
}
