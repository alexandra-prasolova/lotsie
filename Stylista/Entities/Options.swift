//
//  Options.swift
//  Lotsie
//
//  Created by Alexandra Prasolova on 10.03.2021.
//  Copyright © 2021 Alexandra Prasolova. All rights reserved.
//

import Foundation

typealias Option = (name: String, selected: Bool)

enum OrderOption: String, CaseIterable {
    case byName = "By Name"
    case byNameReversed = "By Name Reversed"
}

enum DisplayOption: String, CaseIterable {
    case closeupCollection = "Close-up Collection"
    case collection = "Collection"
    case list = "List"
}

struct Choice {

    enum Kind {
        case order
        case display
    }

    var kind: Kind = .order
    var title: String
    var options: [Option]

}
